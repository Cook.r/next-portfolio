import React from 'react'
import ProjectCard from './ProjectCard'


const ProjectsSection = () => {
    const projectsData = [
        {
          id: 1,
          title: "Cosmos",
          description: "Project 1 description",
          image: "/images/projects/1.png",
          tag: ["All", "Web"],
          gitUrl: "https://gitlab.com/Cook.r/planets",
          previewUrl: "/",
        },
        {
          id: 2,
          title: "Scrumptious",
          description: "Project 2 description",
          image: "/images/projects/2.png",
          tag: ["All", "Web"],
          gitUrl: "https://gitlab.com/Cook.r/scrumptious",
          previewUrl: "/",
        },
        {
          id: 3,
          title: "CarCar",
          description: "Project 3 description",
          image: "/images/projects/3.png",
          tag: ["All", "Web"],
          gitUrl: "https://gitlab.com/Cook.r/carcar",
          previewUrl: "/",
        },
        {
          id: 4,
          title: "JumpSched",
          description: "Project 4 description",
          image: "/images/projects/4.png",
          tag: ["All", "Mobile"],
          gitUrl: "https://gitlab.com/fall-guys/JumpSched",
          previewUrl: "/",
        }
      ];

      
  return (
    <>
    <h2 className="text-center text-4xl font-bold text-white mt-4 mb-8 ">My Projects</h2>
    <div className='grid md:grid-cols-3 gap-8 md:gap-12'>{projectsData.map((project)=> 
    <ProjectCard key={project.id} title={project.title} description={project.description} imgUrl={project.image} gitUrl={project.gitUrl}/>)}
    </div>
    </>
  )
}

export default ProjectsSection
