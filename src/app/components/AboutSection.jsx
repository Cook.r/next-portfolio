"use client"

import {React,useState, useTransition} from 'react'
import Image from 'next/image'
import TabButton from './TabButton'


const Tab_Data = [
    {
        title:"Skills",
        id:"skills",
        content: (
            <ul className='list-disc pl-2 grid grid-cols-2'>
                <li>HTML</li>
                <li>CSS</li>
                <li>JavaScript</li>
                <li>React</li>
                <li>Next.js</li>
                <li>SASS</li>
                <li>Tailwind</li>
            </ul>
        )
    },

    {
        title:"Certifications",
        id:"certifications",
        content:(
            <ul className='list-disc pl-2'>
                <li>AWS</li>
                <li>Frontend guru</li>
            </ul>
        ) 
    },

    {
        title:"Education",
        id:"education",
        content: (
            <ul className='list-disc pl-2'>
                <li>Hack Reactor Immersive Engineering</li>
                <li>Hardvards CS50 Course</li>
            </ul>
        )
    },

]


const AboutSection = () => {

    const [tab, setTab] = useState("skills")
    const [isPending, startTransition] = useTransition()

    const handleTabChange = (id) => {
        startTransition(()=> {
            setTab(id)
        })
    }
  return (
    <section className='text-white '>
    <div className='md:grid md:grid-cols-2 gap-8 items-center py-8 px-4 xl-gap-16 sm:py-16 xl-py-16'>
        <Image src="/images/about-image.png" width={500} height={500}/>
        
        <div className='mt-4 md:mt-0 text-left flex flex-col h-full'>
            <h2 className='text-4xl font-bold text-white mb-4'>About Me</h2>
            <p className='text-base lg:text-lg'>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eum, numquam non quos accusamus dolore, nisi esse atque dicta blanditiis itaque doloribus vitae eius pariatur. Et maiores asperiores vel nobis illum!</p>

            <div className="flex flex-row justify-start mt-8">
                <TabButton selectTab={() => handleTabChange("skills")} active={tab === "skills"}> Skills </TabButton>
                <TabButton selectTab={() => handleTabChange("certifications")} active={tab === "certifications"}> Certifications </TabButton>
                <TabButton selectTab={() => handleTabChange("education")} active={tab === "education"}> Education </TabButton>  </div>
                <div className='mt-8'>{Tab_Data.find((data) => data.id === tab).content} </div>
        </div>

    </div>
    </section>
  )
}

export default AboutSection
